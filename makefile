IMAGE ?= inkscape

.PHONY: all build run

all: build run

build:
	docker build -t $(IMAGE) .

run:
	docker run --rm -ti $(IMAGE)

dev:
	docker run --rm -it -v `pwd`:/code -w /code $(IMAGE) bash
